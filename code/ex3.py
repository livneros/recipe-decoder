import re
import nltk
import random
nltk.download("dependency_treebank")
from nltk.corpus import dependency_treebank
from collections import defaultdict, namedtuple


########helper function ##############


def merge_vectors(vec1, vec2, bool):
    for feature in vec1:
        if feature in vec2.keys():
            if (bool):
                vec1[feature] += vec2[feature]
            else:
                vec1[feature] -= vec2[feature]
    for feature in vec2:
        if feature not in vec1.keys():
            if (bool):
                vec1[feature] = vec2[feature]
            else:
                vec1[feature] = - vec2[feature]
    return vec1





#################### Q1 ###################

data = dependency_treebank.parsed_sents()
trainingSample = data[:int(len(data) * 0.9)]
testSample = data[int(len(data) * 0.9):]


############### Q2 #######################

def featureFunc(father, son, sentenceTree, distenceBool):
        vec = {(father['word'],son['word']):1, (father['tag'],son['tag']):1}
        if(distenceBool):
            distence = abs(father['address'] - son['address'])
            if(distence > 3):
                vec['DISTENCE_4'] = 1
            else:
                vec['DISTENCE_' + str(distence)] = 1
        return vec


############## Q3 ######################

def scoreNode(weight, featureVec):
        ans = 0
        for feature in featureVec:
                if feature in weight.keys():
                        ans += (featureVec[feature] * weight[feature])
        return ans

# turns a tree given from the data, to a vector
def scoreTree(sentence,distenceBool):
        vec = {}
        curVec = {}
        i = 1
        while (sentence.get_by_address(i)['address'] != None):
                curVec.clear()
                node = sentence.get_by_address(i)
                father = sentence.get_by_address(node['head'])
                curVec = featureFunc(father, node, sentence, distenceBool)
                vec = merge_vectors(vec, curVec, True)
                i += 1
        return vec

# turns a tree given from the chu-liu, to a vector
def scoreTreeCho(list,sentence,distenceBool):
    vec = {}
    curVec = {}
    for index in list:
        curVec.clear()
        node = sentence.get_by_address(index)
        father = sentence.get_by_address(list[index][2])
        curVec = featureFunc(father, node, sentence, distenceBool)
        vec = merge_vectors(vec, curVec, True)
    return vec


# chu-liu algorithm
Arc = namedtuple('Arc', ('tail', 'weight', 'head'))

def min_spanning_arborescence(arcs, sink):
    good_arcs = []
    quotient_map = {arc.tail: arc.tail for arc in arcs}
    quotient_map[sink] = sink
    while True:
        min_arc_by_tail_rep = {}
        successor_rep = {}
        for arc in arcs:
            if arc.tail == sink:
                continue
            tail_rep = quotient_map[arc.tail]
            head_rep = quotient_map[arc.head]
            if tail_rep == head_rep:
                continue
            if tail_rep not in min_arc_by_tail_rep or min_arc_by_tail_rep[tail_rep].weight > arc.weight:
                min_arc_by_tail_rep[tail_rep] = arc
                successor_rep[tail_rep] = head_rep
        cycle_reps = find_cycle(successor_rep, sink)
        if cycle_reps is None:
            good_arcs.extend(min_arc_by_tail_rep.values())
            return spanning_arborescence(good_arcs, sink)
        good_arcs.extend(min_arc_by_tail_rep[cycle_rep] for cycle_rep in cycle_reps)
        cycle_rep_set = set(cycle_reps)
        cycle_rep = cycle_rep_set.pop()
        quotient_map = {node: cycle_rep if node_rep in cycle_rep_set else node_rep for node, node_rep in quotient_map.items()}

def find_cycle(successor, sink):
    visited = {sink}
    for node in successor:
        cycle = []
        while node not in visited:
            visited.add(node)
            cycle.append(node)
            node = successor[node]
        if node in cycle:
            return cycle[cycle.index(node):]
    return None

def spanning_arborescence(arcs, sink):
    arcs_by_head = defaultdict(list)
    for arc in arcs:
        if arc.tail == sink:
            continue
        arcs_by_head[arc.head].append(arc)
    solution_arc_by_tail = {}
    stack = arcs_by_head[sink]
    while stack:
        arc = stack.pop()
        if arc.tail in solution_arc_by_tail:
            continue
        solution_arc_by_tail[arc.tail] = arc
        stack.extend(arcs_by_head[arc.tail])
    return solution_arc_by_tail

# prepers the input for the chu-liu algorithm
def sentenceToArc(sentence, weightVec,distenceBool):
    arcs = []
    i = 0
    j = 1
    while (sentence.get_by_address(i)['address'] != None):
        while (sentence.get_by_address(j)['address'] != None):
            if ( i != j):
                node = sentence.get_by_address(j)
                father = sentence.get_by_address(i)
                score = scoreNode(weightVec, featureFunc(father, node, sentence,distenceBool))
                arcs.append(Arc(j,score * (-1),i))
            j += 1
        i += 1
        j = 1
    return arcs

# the 'main' of the question
def parser(training, distenceBool):
    iterations = 2
    weight = {}
    sumWeight = {}
    for n in range(iterations):
        random.shuffle(training)
        for sentence in training:
            tree = min_spanning_arborescence(sentenceToArc(sentence,weight,distenceBool),0)
            weight = merge_vectors(weight, scoreTree(sentence,distenceBool), True)
            weight = merge_vectors(weight, scoreTreeCho(tree, sentence,distenceBool), False)
            sumWeight = merge_vectors(sumWeight, weight,True)
    for i in sumWeight:
        sumWeight[i] /= iterations*len(training)
    return sumWeight


################## Q4 ##################
def compute(test, weight,distenceBool):
    score = 0
    for sentence in test:
        tree = min_spanning_arborescence(sentenceToArc(sentence,weight,distenceBool),0)
        i = 1
        count = 0
        correctConut = 0
        while sentence.get_by_address(i)['address'] != None:
            fatherIndex = sentence.get_by_address(i)['head']
            count += 1
            if(fatherIndex == tree[i][2]):
                correctConut += 1
            i += 1
        score += (correctConut/count)
    return score / len(test)

### WITHOUT DISTANCE FEATURE:
w = parser(trainingSample, False)
print(compute(testSample,w,False))

### WITH DISTANCE FEATURE:

# w = parser(trainingSample, True)
# print(compute(testSample,w,True))


# without distance feature: 0.3543464332455
# with distance feature: 0.5719421900519874